export type ILoginRequestBody = {
  username: string;
  password: string;
};

export type ILoginResponse = {
  token: string;
  user: {
    id: number;
    username: string;
    is_student: boolean;
    is_teacher: boolean;
  };
};
