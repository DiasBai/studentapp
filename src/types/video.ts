export type IVideo = {
  id: number;
  title: string;
  video_url: string;
  audio_file: string;
  date: string;
  view_count: number;
  students_viewed: [number];
};
