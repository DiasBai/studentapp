export type ILesson = {
  id: number;
  zoom_link: string;
  teacher: {
    id: number;
    user: {
      id: number;
      username: string;
      is_student: boolean;
      is_teacher: boolean;
    };
  };
  start_time: string;
  end_time: string;
  day_of_week: string;
  students: [number];
};
