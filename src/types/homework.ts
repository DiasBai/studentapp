export type IHomework = {
  id: number;
  student_topic: {
    id: number;
    student: number;
    lesson: number;
    topic: string;
    date: string;
    teacher: number;
    attended: boolean;
    sura: number;
    repeat_count: number;
  };
  assignment: string;
  due_date: string;
  grade: number;
  comment: string;
};
