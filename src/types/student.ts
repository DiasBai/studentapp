export type IStudent = {
  id: number;
  user: {
    id: number;
    username: string;
    is_student: boolean;
    is_teacher: boolean;
  };
  teacher: {
    id: number;
    user: {
      id: number;
      username: string;
      is_student: boolean;
      is_teacher: boolean;
    };
  };
  purpose: string;
};
