export type IMistake = {
  id: number;
  homework: number;
  description: string;
  date: string;
  is_corrected: boolean;
};
