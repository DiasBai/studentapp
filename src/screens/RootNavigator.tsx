import React, {useEffect} from 'react';
import {useAppSelector, useAppDispatch} from '../store';
import LoginScreen from './LoginScreen';
import {createStackNavigator} from '@react-navigation/stack';
import TabNavigator from './TabNavigator.tsx';
import {api} from '../store/api'; // Импортируйте API для использования метода invalidateTags

const Stack = createStackNavigator();

export default function RootNavigator() {
  const token = useAppSelector(state => state.auth.token);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (token) {
      // Если токен изменился (пользователь авторизовался), обновляем данные
      dispatch(
        api.util.invalidateTags([
          'Homeworks',
          'Mistakes',
          'Lessons',
          'Students',
          'Video',
        ]),
      );
    }
  }, [token, dispatch]);

  if (token) {
    return (
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Main" component={TabNavigator} />
      </Stack.Navigator>
    );
  }
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={LoginScreen} />
    </Stack.Navigator>
  );
}
