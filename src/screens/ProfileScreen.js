import {Button, Text, View} from 'react-native';
import * as React from 'react';
import {useCallback} from 'react';
import {useAppDispatch} from '../store';
import {logOutAction} from '../store/slices/authSlice';

const ProfileScreen = () => {
  const dispatch = useAppDispatch();
  const onPress = useCallback(() => {
    dispatch(logOutAction());
  }, [dispatch]);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
      }}>
      <Text>Выход из Профиля</Text>

      <Button title={'Выйти'} onPress={onPress} />
    </View>
  );
};
export default ProfileScreen;
