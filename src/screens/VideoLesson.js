import React, {useState} from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import YoutubeIframe from 'react-native-youtube-iframe';

const VideoLesson = ({route}) => {
  const {videoUrl, title} = route.params;
  const [loading, setLoading] = useState(true);

  // Extract the video ID from the URL
  const videoId = videoUrl.split('v=')[1].split('&')[0];

  return (
    <View style={styles.container}>
      <View style={styles.videoContainer}>
        {loading && (
          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        )}
        <YoutubeIframe
          height={230}
          play={true}
          videoId={videoId}
          onReady={() => setLoading(false)}
        />
      </View>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  videoContainer: {
    position: 'relative',
    height: 230,
  },
  loadingContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000',
    paddingHorizontal: 10,
    marginTop: 20,
  },
});

export default VideoLesson;
