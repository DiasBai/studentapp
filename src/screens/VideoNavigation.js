import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import VideoScreen from './VideoScreen';
import VideoLesson from './VideoLesson';

const Stack = createStackNavigator();

const VideoNavigation = () => {
  return (
    <Stack.Navigator initialRouteName="VideoScreen">
      <Stack.Screen
        name="VideoScreen"
        component={VideoScreen}
        options={{title: 'Видео Уроки'}}
      />
      <Stack.Screen
        name="VideoLesson"
        component={VideoLesson}
        options={{title: 'Видео Урок'}}
      />
    </Stack.Navigator>
  );
};

export default VideoNavigation;
