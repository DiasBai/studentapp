import React from 'react';
import HomeScreen from './HomeScreen';
import House from '../icons/House';
import MistakeScreen from './MistakeScreen';
import Mistake from '../icons/Mistake';
import VideoNavigation from './VideoNavigation';
import Video from '../icons/Video';
import ProfileScreen from './ProfileScreen';
import Sett from '../icons/Sett';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

export default function TabNavigator() {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarShowLabel: false,
        tabBarStyle: {
          borderTopWidth: 0,
          elevation: 0,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerShown: false,
          tabBarIcon: ({focused}) => <House focused={focused} />,
        }}
      />
      <Tab.Screen
        name="Ошибки"
        component={MistakeScreen}
        options={{
          tabBarIcon: ({focused}) => <Mistake focused={focused} />,
        }}
      />
      <Tab.Screen
        name="VideoNavigation"
        component={VideoNavigation}
        options={{
          headerShown: false,
          tabBarIcon: ({focused}) => <Video focused={focused} />,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({focused}) => <Sett focused={focused} />,
        }}
      />
    </Tab.Navigator>
  );
}
