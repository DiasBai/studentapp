import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {
  useGetHomeworksQuery,
  useGetLessonsQuery,
  useGetStudentsQuery,
} from '../store/api';
import {differenceInSeconds, setHours, setMinutes} from 'date-fns';
import SampleSVG from '../icons/SampleSVG';

const today = new Date();

const getDateFromTimeString = (timestring /* '16:25' */) => {
  const [hours, minutes] = timestring.split(':').map(Number);
  return setMinutes(setHours(today, hours), minutes);
};

const getTwoDigitNum = num => {
  if (num >= 10) {
    return num.toString();
  }
  return `0${num}`;
};

const getDiffInHourMinutes = (date1, date2) => {
  const _seconds = Math.abs(differenceInSeconds(date1, date2));
  const hours = Math.floor(_seconds / 3600);
  const minutes = Math.floor((_seconds - hours * 3600) / 60);
  const seconds = _seconds - hours * 3600 - minutes * 60;
  return `${getTwoDigitNum(hours)}:${getTwoDigitNum(minutes)}:${getTwoDigitNum(
    seconds,
  )}`;
};

const daysOfWeek = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];

const LeftTime = ({startTime}) => {
  const [leftTime, setLeftTime] = useState(
    getDiffInHourMinutes(today, getDateFromTimeString(startTime)),
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setLeftTime(
        getDiffInHourMinutes(new Date(), getDateFromTimeString(startTime)),
      );
    }, 1000);

    return () => clearTimeout(timer);
  }, [startTime, leftTime]);

  return <Text>{leftTime}</Text>;
};

const HomeScreen = () => {
  const {data: lessons = []} = useGetLessonsQuery();
  const {data: homeworks = []} = useGetHomeworksQuery();
  const {data: students = []} = useGetStudentsQuery();

  const groupLessonsByDay = () => {
    return daysOfWeek
      .map(day => ({
        type: 'lesson',
        day,
        lessons: lessons.filter(lesson => lesson.day_of_week === day),
      }))
      .filter(group => group.lessons.length > 0); // Filter out days with no lessons
  };

  const renderLessonItem = ({item}) => (
    <View style={styles.lessonItem}>
      <Text style={styles.lessonTime}>
        {item.start_time.split(':').slice(0, 2).join(':')} -{' '}
        {item.end_time.split(':').slice(0, 2).join(':')}
      </Text>
      <Text style={styles.lessonTitle}>
        Учитель: {item.teacher.user.username}
      </Text>
    </View>
  );

  const renderDayItem = ({item}) => (
    <View style={styles.dayContainer}>
      <Text style={styles.dayTitle}>{item.day}</Text>
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={item.lessons}
        renderItem={renderLessonItem}
        keyExtractor={lesson => lesson.id.toString()}
      />
    </View>
  );

  const renderHomeworkItem = ({item}) => (
    <View style={styles.homeworkItem}>
      <Text style={styles.homeworkTopic}>{item.student_topic.topic}</Text>
      <Text style={styles.homeworkComment}>{item.comment}</Text>
    </View>
  );

  const mergedData = [
    {type: 'header', key: 'header'},
    ...groupLessonsByDay().map(day => ({...day, key: day.day})),
    {type: 'homeworkHeader', key: 'homeworkHeader'}, // добавляем заголовок для домашнего задания
    ...homeworks.map(homework => ({
      ...homework,
      type: 'homework',
      key: `homework-${homework.id}`,
    })),
  ];

  const renderItem = ({item}) => {
    switch (item.type) {
      case 'header':
        return (
          <View>
            <View style={{flexDirection: 'row', gap: 10}}>
              <Text style={[styles.lessonTime, {fontWeight: 'bold'}]}>
                Цель заучивания:
              </Text>
              <Text style={styles.lessonTime}>{students?.[0]?.purpose}</Text>
            </View>

            <TouchableOpacity
              style={styles.card}
              onPress={() => Linking.openURL(lessons?.[0]?.zoom_link)}>
              <View style={{alignItems: 'center'}}>
                <Text
                  style={[
                    styles.lessonTime,
                    {color: '#fff', fontWeight: 'bold'},
                  ]}>
                  Перейти на следующий урок:
                </Text>
                <Text
                  style={[
                    styles.lessonTime,
                    {color: '#fff', fontWeight: 'bold'},
                  ]}>
                  {lessons?.[0]?.start_time ? (
                    <LeftTime startTime={lessons?.[0]?.start_time} />
                  ) : null}
                </Text>
              </View>
              <SampleSVG />
            </TouchableOpacity>
            <Text style={[styles.homeworkHeaderText, {borderBottomWidth: 1}]}>
              Расписание уроков
            </Text>
          </View>
        );
      case 'lesson':
        return renderDayItem({item});
      case 'homeworkHeader':
        return (
          <View style={styles.homeworkHeaderContainer}>
            <Text style={styles.homeworkHeaderText}>Домашнее задание:</Text>
          </View>
        );
      case 'homework':
        return renderHomeworkItem({item});
      default:
        return null;
    }
  };

  return (
    <FlatList
      data={mergedData}
      renderItem={renderItem}
      keyExtractor={item => item.key}
      contentContainerStyle={styles.container}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: '#fff',
  },
  card: {
    marginVertical: 20,
    backgroundColor: 'rgba(3,98,200,0.72)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    padding: 5,
  },
  dayContainer: {
    borderBottomWidth: 1,
    borderColor: '#eee',
    marginVertical: 10,
  },
  dayTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
    marginBottom: 5,
  },
  lessonItem: {
    borderColor: '#F4F4F4',
    padding: 10,
  },
  lessonTime: {
    fontSize: 18,
    color: '#000',
  },
  lessonTitle: {
    fontSize: 16,
    color: '#000',
  },
  homeworkHeaderContainer: {
    marginVertical: 10,
  },
  homeworkHeaderText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
  },
  homeworkItem: {
    backgroundColor: '#f9f9f9',
    padding: 10,
    borderRadius: 5,
    marginTop: 10,
  },
  homeworkTopic: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
  },
  homeworkComment: {
    fontSize: 16,
    color: '#000',
  },
});

export default HomeScreen;
