import React from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';
import {useGetHomeworksQuery, useGetMistakesQuery} from '../store/api';
import Correct from '../icons/Correct';
import Incorrect from '../icons/Incorrect';

const MistakeScreen = () => {
  const {data: mistakes = []} = useGetMistakesQuery();
  const {data: homeworks = []} = useGetHomeworksQuery();

  const renderItem = ({item}) => {
    const homework = homeworks.find(hw => hw.id === item.homework);
    return (
      <View style={styles.mistakeContainer}>
        {homework && (
          <Text style={styles.assignmentText}>{homework.assignment}</Text>
        )}
        <View style={styles.mistakeItem}>
          <Text style={styles.mistakeDescription}>{item.description}</Text>
          {item.is_corrected ? <Correct /> : <Incorrect />}
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={mistakes}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: '#fff',
  },
  mistakeContainer: {
    marginVertical: 10,
  },
  assignmentText: {
    fontSize: 16,
    color: '#000',
    marginBottom: 5,
    fontWeight: 'bold',
  },
  mistakeItem: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#eee',
  },
  mistakeDescription: {
    width: 300,
    color: '#000',
    fontSize: 16,
  },
});

export default MistakeScreen;
