import React, {useState} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import {useGetVideoQuery} from '../store/api';
import {useNavigation} from '@react-navigation/native';
import Play from '../icons/Play';
import Search from '../icons/Search';

const VideoScreen = () => {
  const {data: videos} = useGetVideoQuery();
  const navigation = useNavigation();
  const [searchText, setSearchText] = useState('');

  const filteredVideos = videos?.filter(video =>
    video.title.toLowerCase().includes(searchText.toLowerCase()),
  );

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.videoItem}
      onPress={() =>
        navigation.navigate('VideoLesson', {
          videoUrl: item.video_url,
          title: item.title,
        })
      }>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Text style={styles.circle}>{item.id}</Text>
        <Text style={styles.videoTitle}>{item.title}</Text>
      </View>

      <View style={[styles.circle, {paddingHorizontal: 6, paddingVertical: 6}]}>
        <Play />
      </View>
    </TouchableOpacity>
  );

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <View style={[styles.card, styles.borderShadow]}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Search />
            <TextInput
              style={styles.textInput}
              placeholder={'Поиск видео'}
              value={searchText}
              onChangeText={setSearchText}
            />
          </View>
        </View>
        <View style={styles.card}>
          <FlatList
            data={filteredVideos}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: '#fff',
  },
  borderShadow: {
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  videoItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#eee',
    paddingVertical: 20,
  },
  videoTitle: {
    color: '#000',
    paddingHorizontal: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
  card: {
    borderRadius: 12,
    borderColor: '#eee',
    backgroundColor: '#fff',
    marginVertical: 10,
    paddingHorizontal: 10,
  },
  circle: {
    borderRadius: 100,
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: '#eee',
  },
  textInput: {
    flex: 1,
    marginLeft: 10,
    fontSize: 16,
  },
});

export default VideoScreen;
