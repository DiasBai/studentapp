import React from 'react';
import Svg, {Path} from 'react-native-svg';

export default function Play({size = 12}) {
  return (
    <Svg width={size} height={size} viewBox="0 0 14 16">
      <Path
        d="M.333 8V5.033c0-3.683 2.608-5.191 5.8-3.35l2.575 1.484 2.575 1.483c3.192 1.842 3.192 4.858 0 6.7l-2.575 1.483-2.575 1.484c-3.192 1.841-5.8.333-5.8-3.35V8z"
        fill="#000"
      />
    </Svg>
  );
}
