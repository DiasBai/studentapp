import React from 'react';
import Svg, {ClipPath, Defs, G, Mask, Path, Rect} from 'react-native-svg';

export default function Video({focused}) {
  return (
    <Svg width={24} height={24} viewBox="0 0 682.66669 682.66669">
      <Defs>
        <ClipPath clipPathUnits="userSpaceOnUse" id="b">
          <Path d="M0 512h512V0H0z" />
        </ClipPath>
      </Defs>
      <Mask id="a">
        <Rect x={0} y={0} width="100%" height="100%" fill="#fff" />
        <Path
          transform="matrix(1.33333 0 0 -1.33333 0 682.667) translate(95 40)"
          d="M0 0c0 22.092 17.908 40 40 40S80 22.092 80 0 62.092-40 40-40 0-22.092 0 0"
          stroke={focused ? '#000' : 'rgba(0,0,0,0.35)'}
          fillOpacity={1}
          fillRule="nonzero"
        />
      </Mask>
      <G mask="url(#a)">
        <G
          clipPath="url(#b)"
          transform="matrix(1.33333 0 0 -1.33333 0 682.667)">
          <Path
            d="M0 0l85.854 49.819c12.707 7.373 12.707 25.792 0 33.165L0 132.803c-12.716 7.379-28.616-1.836-28.616-16.583V16.583C-28.616 1.836-12.716-7.379 0 0z"
            transform="translate(235.616 250.599)"
            fill="none"
            stroke="#000"
            strokeWidth={40}
            strokeLinecap="butt"
            strokeLinejoin="round"
            strokeMiterlimit={10}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <Path
            d="M0 0c0-33.137-26.863-60-60-60h-352c-33.137 0-60 26.863-60 60v230c0 33.137 26.863 60 60 60h352c33.137 0 60-26.863 60-60V101"
            transform="translate(492 202)"
            fill="none"
            stroke="#000"
            strokeWidth={40}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit={10}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <Path
            d="M0 0h-115"
            transform="translate(135 41)"
            fill="none"
            stroke="#000"
            strokeWidth={40}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit={10}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <Path
            d="M0 0h-257"
            transform="translate(492 41)"
            fill="none"
            stroke="#000"
            strokeWidth={40}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit={10}
            strokeDasharray="none"
            strokeOpacity={1}
          />
          <Path
            d="M0 0c0 22.092 17.908 40 40 40S80 22.092 80 0 62.092-40 40-40 0-22.092 0 0"
            transform="translate(95 40)"
            fill="#000"
            fillOpacity={1}
            fillRule="nonzero"
            stroke="none"
          />
        </G>
      </G>
    </Svg>
  );
}
