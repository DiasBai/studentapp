import React from 'react';
import Svg, {Path} from 'react-native-svg';
import {View} from 'react-native';

const SampleSVG = () => (
  <View style={{paddingLeft: 20}}>
    <Svg width={24} height={24} viewBox="0 0 7 12" fill="none">
      <Path
        d="M1.198 10.62L5 6.817c.449-.45.449-1.184 0-1.634L1.198 1.38"
        stroke="#fff"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  </View>
);

export default SampleSVG;
