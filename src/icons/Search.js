import React from 'react';
import Svg, {ClipPath, Defs, G, Path} from 'react-native-svg';
import {View} from 'react-native';

export default function Search({size = 20, color = '#000'}) {
  return (
    <View style={{paddingRight: 5}}>
      <Svg width={size} height={size} viewBox="0 0 20 20" fill="none">
        <G
          clipPath="url(#clip0_4_142)"
          stroke={color}
          strokeLinecap="round"
          strokeLinejoin="round">
          <Path d="M8.457 16.2a7.743 7.743 0 100-15.486 7.743 7.743 0 000 15.486zM19.286 19.286l-5.357-5.357" />
        </G>
        <Defs>
          <ClipPath id="clip0_4_142">
            <Path fill="#fff" d="M0 0H20V20H0z" />
          </ClipPath>
        </Defs>
      </Svg>
    </View>
  );
}
