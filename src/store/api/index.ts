import {
  BaseQueryFn,
  FetchArgs,
  fetchBaseQuery,
  FetchBaseQueryError,
} from '@reduxjs/toolkit/query';
import {createApi} from '@reduxjs/toolkit/query/react';
import {ILoginRequestBody, ILoginResponse} from '../../types/api.ts';
import {RootState} from '../index.ts';
import {IVideo} from '../../types/video.ts';
import {ILesson} from '../../types/lesson.ts';
import {IStudent} from '../../types/student.ts';
import {IMistake} from '../../types/mistake.ts';
import {IHomework} from '../../types/homework.ts';

const baseUrl = 'https://jacksylyk.ru/api';

const baseQuery = fetchBaseQuery({
  baseUrl,
  prepareHeaders: (headers, {getState}) => {
    const state = getState() as RootState;
    headers.set('Content-Type', 'application/json');
    if (state.auth.token) {
      headers.set('Authorization', `Token ${state.auth.token}`);
    }
    return headers;
  },
});

const baseQueryWithReAuth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  const result = await baseQuery(args, api, extraOptions);
  return result;
};

export const api = createApi({
  reducerPath: 'api',
  baseQuery: baseQueryWithReAuth,
  tagTypes: ['Homeworks', 'Mistakes', 'Lessons', 'Students', 'Video'], // Добавьте типы тэгов
  endpoints: builder => ({
    login: builder.mutation<ILoginResponse, ILoginRequestBody>({
      query: body => ({
        url: 'users/login/',
        method: 'POST',
        body,
      }),
    }),
    getHomeworks: builder.query<IHomework[], void>({
      query: () => ({
        url: 'homeworks/',
      }),
      providesTags: ['Homeworks'], // Добавьте тэг для кэширования
    }),
    getLessons: builder.query<ILesson[], void>({
      query: () => ({
        url: 'lessons/actual/',
      }),
      providesTags: ['Lessons'], // Добавьте тэг для кэширования
    }),
    getStudents: builder.query<IStudent[], void>({
      query: () => ({
        url: 'students/',
      }),
      providesTags: ['Students'], // Добавьте тэг для кэширования
    }),
    getVideo: builder.query<IVideo[], void>({
      query: () => ({
        url: 'knowledge-base/',
      }),
      providesTags: ['Video'], // Добавьте тэг для кэширования
    }),
    getMistakes: builder.query<IMistake[], void>({
      query: () => ({
        url: 'mistakes/',
      }),
      providesTags: ['Mistakes'], // Добавьте тэг для кэширования
    }),
    // new endpoints
  }),
});

export const {
  useLoginMutation,
  useGetHomeworksQuery,
  useGetLessonsQuery,
  useGetStudentsQuery,
  useGetVideoQuery,
  useGetMistakesQuery,
} = api;
