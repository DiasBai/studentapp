import {createSlice, PayloadAction} from '@reduxjs/toolkit';

type AuthState = {
  token: string;
};

const authInitialState: AuthState = {
  token: '',
};

const authSlice = createSlice({
  name: 'auth',
  initialState: authInitialState,
  reducers: {
    setTokenAction(state, action: PayloadAction<string>) {
      state.token = action.payload;
    },
    logOutAction(state) {
      state.token = '';
    },
  },
  extraReducers: builder => {
    builder.addCase('logOutAction', () => {
      console.log('logged out');
    });
  },
});

export const {setTokenAction, logOutAction} = authSlice.actions;
const authReducer = authSlice.reducer;
export default authReducer;
